## Dockerfile 2 fase para optimizar images
## 1do Stage: builder de Fat Jar
## 2er Stage: Optimizacion de image separando por layers
## https://docs.spring.io/spring-boot/docs/current/reference/html/container-images.html#container-images.efficient-images

FROM eclipse-temurin:11 AS builder
WORKDIR source
ARG JAR_FILE=/target/bi-bank-service-*.jar
COPY ${JAR_FILE} app.jar
RUN java -Djarmode=layertools -jar app.jar extract

FROM adoptopenjdk:11
RUN useradd spring
USER spring
WORKDIR app
COPY --from=builder source/dependencies/ ./
COPY --from=builder source/spring-boot-loader/ ./
COPY --from=builder source/snapshot-dependencies/ ./
COPY --from=builder source/application/ ./
ENTRYPOINT ["java", "org.springframework.boot.loader.JarLauncher"]
