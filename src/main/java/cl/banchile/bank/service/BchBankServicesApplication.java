package cl.banchile.bank.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BchBankServicesApplication {

    public static void main(String[] args) {
        SpringApplication.run(BchBankServicesApplication.class, args);
    }

}
