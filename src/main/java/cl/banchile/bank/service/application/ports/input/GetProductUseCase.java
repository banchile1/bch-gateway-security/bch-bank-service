package cl.banchile.bank.service.application.ports.input;


import cl.banchile.bank.service.domain.model.Product;

public interface GetProductUseCase {

    Product getProductById(Long id);

}
