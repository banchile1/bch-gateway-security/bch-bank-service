package cl.banchile.bank.service.application.ports.ouput;


import cl.banchile.bank.service.domain.model.Product;

import java.util.Optional;

public interface ProductOutputPort {

    Product saveProduct(Product product);

    Optional<Product> getProductById(Long id);

}
