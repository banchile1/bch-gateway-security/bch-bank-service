package cl.banchile.bank.service.domain.service;


import cl.banchile.bank.service.application.ports.input.CreateProductUseCase;
import cl.banchile.bank.service.application.ports.input.GetProductUseCase;
import cl.banchile.bank.service.application.ports.ouput.ProductOutputPort;
import cl.banchile.bank.service.domain.event.ProductCreatedEvent;
import cl.banchile.bank.service.domain.exception.ProductNotFound;

import cl.banchile.bank.service.domain.model.Product;
import cl.banchile.bank.service.infrastructure.adapters.output.persistence.ProductPersistenceAdapter;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ProductService implements CreateProductUseCase, GetProductUseCase {

    private final ProductOutputPort productOutputPort;



    public ProductService(ProductPersistenceAdapter productPersistenceAdapter) {
        this.productOutputPort =productPersistenceAdapter;
    }

    @Override
    public Product createProduct(Product product) {
        product = productOutputPort.saveProduct(product);
        return product;
    }

    @Override
    public Product getProductById(Long id) {
        return productOutputPort.getProductById(id).orElseThrow(() -> new ProductNotFound("Product not found with id " + id));
    }

}
