package cl.banchile.bank.service.infrastructure.adapters.config;

import cl.banchile.bank.service.domain.service.ProductService;
import cl.banchile.bank.service.infrastructure.adapters.output.persistence.ProductPersistenceAdapter;
import cl.banchile.bank.service.infrastructure.adapters.output.persistence.mapper.ProductPersistenceMapper;
import cl.banchile.bank.service.infrastructure.adapters.output.persistence.repository.ProductRepository;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfiguration {

    @Bean
    public ProductPersistenceAdapter productPersistenceAdapter(ProductRepository productRepository, ProductPersistenceMapper productPersistenceMapper) {
        return new ProductPersistenceAdapter(productRepository, productPersistenceMapper);
    }
/*
    @Bean
    public ProductEventPublisherAdapter productEventPublisherAdapter(ApplicationEventPublisher applicationEventPublisher) {
        return new ProductEventPublisherAdapter(applicationEventPublisher);
    }
*/
    @Bean
    public ProductService productService(ProductPersistenceAdapter productPersistenceAdapter) {
        return new ProductService(productPersistenceAdapter);
    }

}
