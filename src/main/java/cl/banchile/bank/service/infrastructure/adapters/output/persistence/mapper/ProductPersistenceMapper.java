package cl.banchile.bank.service.infrastructure.adapters.output.persistence.mapper;


import cl.banchile.bank.service.domain.model.Product;
import cl.banchile.bank.service.infrastructure.adapters.output.persistence.entity.ProductEntity;
import org.mapstruct.Mapper;

@Mapper
public interface ProductPersistenceMapper {

    ProductEntity toProductEntity(Product product);

    Product toProduct(ProductEntity productEntity);

}
