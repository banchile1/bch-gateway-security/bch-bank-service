package cl.banchile.bank.service.infrastructure.adapters.input.rest.mapper;


import cl.banchile.bank.service.domain.model.Product;
import cl.banchile.bank.service.infrastructure.adapters.input.rest.data.request.ProductCreateRequest;
import cl.banchile.bank.service.infrastructure.adapters.input.rest.data.response.ProductCreateResponse;
import cl.banchile.bank.service.infrastructure.adapters.input.rest.data.response.ProductQueryResponse;
import org.mapstruct.Mapper;

@Mapper
public interface ProductRestMapper {

    Product toProduct(ProductCreateRequest productCreateRequest);

    ProductCreateResponse toProductCreateResponse(Product product);

    ProductQueryResponse toProductQueryResponse(Product product);

}
