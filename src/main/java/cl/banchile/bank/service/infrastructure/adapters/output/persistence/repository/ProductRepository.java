package cl.banchile.bank.service.infrastructure.adapters.output.persistence.repository;

import cl.banchile.bank.service.infrastructure.adapters.output.persistence.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity, Long> {

}
